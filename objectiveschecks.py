#!/usr/bin/env python3

__author__ = 'Margus Ernits, Erki Naumanis'

import configparser
import requests
import sys
import json
import argparse
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parser = argparse.ArgumentParser(
    prog='ObjectiveChecks.py',
    usage='%(prog)s [options]',
    description="""Delivers check results to VTA""")
parser.add_argument('-d', type=str, default='NaN', help='Name of the check as defined in VTA.', required=True)
parser.add_argument('-s', type=int, default=0, help='Score given to user for VTA.')
parser.add_argument('-t', type=str, required=False, help="Data to be written to VTA if Step|Objective allows.")
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-y', action='store_true', dest='objective', help='Check completed successfully.')
group.add_argument('-n', action='store_false', dest='objective', help='Check failed.')
args = parser.parse_args()

config_filename = '/root/running/lab.ini'
print('Reading configuration from:', config_filename)
config = configparser.ConfigParser()
config.read(config_filename)


try:
    ta_key = config.get('LAB', 'LAB_TA_KEY')
    virtualta_hostname = config.get('LAB', 'LAB_VIRTUALTA_HOSTNAME')

    # get the userid from API file
    uid_file = '/root/running/routerapi_vtainfo.json' ## TODO where is this file and info
    uid_data = json.load(open(uid_file))
    lab_id = uid_data['uri'].split('/')[4]
    uid = uid_data['uri'].split('/')[5]
    attempt_id = uid_data['_id']

except Exception:
    print(" Exception no Ini file or LAB section inside ini file or wrong key values")
    sys.exit(1)

payload = {
    "api_key": ta_key,
    "_id": attempt_id,
    "oname": args.d,
    "score": args.s,
    "done": args.objective
    }


if args.t:
    payload["description"] = args.t

# Retry mechanism from
#  https://www.peterbe.com/plog/best-practice-with-retries-with-requests
#  https://stackoverflow.com/questions/15431044/can-i-set-max-retries-for-requests-request

s = requests.Session()
retries = Retry(total=5,
                backoff_factor=0.1,
                status_forcelist=[500, 502, 503, 504])

s.mount('http://', HTTPAdapter(max_retries=retries))
s.mount('https://', HTTPAdapter(max_retries=retries))

# Send Obj data to VTA
r = s.put(virtualta_hostname + '/api/v2/labuser_any', json=payload, verify=False)

