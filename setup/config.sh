#!/usr/bin/env bash
datafile="/root/running/lab_data.ini"
touch ${datafile}
export CLIENTIP="192.168.0.100/24"
export SERVERIPS="192.168.0.200/24 192.168.0.201/24"
export ROUTERIP="192.168.0.254"

# Lab name cannot contain any spaces
export LAB_NAME="Example-LAB"

counter=10

while [ ${counter} -gt 0  ]; do

curl -H "accept: application/json" -X POST "$(dmidecode -s system-product-name)/labinfo?uuid=$(dmidecode -s system-version)" > ${datafile}

        if [ $? -eq 0 ]; then
                break
        fi
        sleep 2
	$((counter--))
done


# export LAB_NAME=$(cat /tmp/lab_data | jq -r '.lab.name')
export LAB_USERNAME=$(cat ${datafile} | jq -r '.user.username')
export LAB_TA_KEY=$(cat ${datafile} | jq -r '.lab.lab_token')
export LAB_VIRTUALTA_HOSTNAME=$(cat ${datafile} | jq -r '.assistant.uri')
export USER_FULLNAME=$(cat ${datafile} | jq -r '.user.name')
export USER_KEY=$(cat ${datafile} | jq -r '.user.user_key')
export LAB_ID=$(cat ${datafile} | jq -r '.lab.lab_hash')

cat > /root/running/lab.ini << EOF
[LAB]
ta_key = $LAB_TA_KEY
virtualta_hostname = $LAB_VIRTUALTA_HOSTNAME
lab_id = $LAB_ID
EOF

